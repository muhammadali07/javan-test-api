from sqlalchemy import Column, String, BigInteger, Text, DateTime
from datetime import datetime
from service import Base

class Keluarga(Base):
    __tablename__ = 'keluarga'
    id = Column(BigInteger, primary_key=True)
    keluarga_name = Column(String(50), nullable=False)
    keluarga_anak = Column(String(50), nullable=False)
    jenis_kelamin_anak = Column(String(1))
    keluarga_cucu = Column(String(50))
    keluarga_jenis_kelamin_cucu = Column(String(1))
    created_at = Column(DateTime, default=datetime.now())