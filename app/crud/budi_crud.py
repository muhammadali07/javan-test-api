from models import Keluarga, budi_models
from schema import BudiFamily, UpdateCucuBudi, DeleteCucuBudi
from sqlalchemy.future import select
from sqlalchemy import and_, or_, desc, update, delete
from sqlalchemy.ext.asyncio import AsyncSession
from datetime import datetime
from fastapi.encoders import jsonable_encoder
from service import ResponseOutCustom, loging
from graphviz import Digraph,Graph

log = loging()

async def create_data_family(request: BudiFamily , db: AsyncSession):
    async with db as session:
        try:
            data = Keluarga(
                keluarga_name = request.keluarga_name,
                keluarga_anak = request.keluarga_anak,
                jenis_kelamin_anak = request.jenis_kelamin_anak,
                keluarga_cucu = request.keluarga_cucu,
                keluarga_jenis_kelamin_cucu = request.keluarga_jenis_kelamin_cucu,
                created_at= datetime.now()
            )
            session.add(data)
            await session.commit()

            return ResponseOutCustom(message_id="00", status="Data Family Successfully", list=request)
        
        except Exception as e:
            return ResponseOutCustom(message_id="03", status=f"{str(e)}", list_data=[])


async def get_data_anak_budi(keyword, db: AsyncSession):
    async with db as session:
        try:
            terms = []
            if keyword not in (None, [],''):
                terms.append((Keluarga.keluarga_anak.ilike(f'%{keyword}%')))

            if terms not in (None, []):
                query_stmt = select(budi_models.Keluarga).filter(and_(Keluarga.keluarga_name =='Budi', *(terms))).order_by(desc(Keluarga.created_at))
            else:
                query_stmt = select(budi_models.Keluarga).where(Keluarga.keluarga_name =='Budi').order_by(desc(Keluarga.created_at))
            
            log.info(query_stmt)
            proxy_rows = await session.execute(query_stmt)
            result = proxy_rows.scalars().all()
            datas = jsonable_encoder(result)
            anak = []
            for i in datas:
                if i['keluarga_anak'] not in anak:
                    anak.append(i['keluarga_anak'])
                else:
                    pass

            status = 'Data anak Budi' if datas not in (None, []) else 'Data tidak ditemukan'

            return ResponseOutCustom(message_id="00", status=status, list_data=anak)
        
        except Exception as e:
            return ResponseOutCustom(message_id="03", status=f'{e}', list_data=[])

async def get_data_family(limit, page, keyword, db: AsyncSession):
    async with db as session:
        try:
            offset = page * limit
            terms = []

            if keyword not in (None, [],''):
                terms.append(
                    or_(
                        (Keluarga.keluarga_anak.ilike(f'%{keyword}%')),
                        (Keluarga.keluarga_cucu.ilike(f'%{keyword}%'))
                    )
                )
                
            if terms not in (None, []):
                query_stmt = select(Keluarga).filter(*(terms)).limit(limit).offset(offset).order_by(desc(Keluarga.created_at))
            else:
                query_stmt = select(Keluarga).limit(limit).offset(offset).order_by(desc(Keluarga.created_at))
            
            proxy_rows = await session.execute(query_stmt)
            result = proxy_rows.scalars().all()
            datas = jsonable_encoder(result)

            status = 'Data Family Budi' if datas not in (None, []) else 'Data tidak ditemukan'

            return ResponseOutCustom(message_id="00", status=status, list_data=datas)
        
        except Exception as e:
            return ResponseOutCustom(message_id="03", status=f'{e}', list_data=[])
        
async def get_data_cucu_budi(keyword, db: AsyncSession):
    async with db as session:
        try:
            terms = []
            if keyword not in (None, [],''):
                terms.append(
                    or_(
                        (Keluarga.keluarga_anak.ilike(f'%{keyword}%')),
                        (Keluarga.keluarga_cucu.ilike(f'%{keyword}%')),
                        (Keluarga.keluarga_jenis_kelamin.ilike(f'%{keyword}%')),
                    )
                )
            if terms not in (None, []):
                query_stmt = select(budi_models.Keluarga).filter(and_(Keluarga.keluarga_name =='Budi', *(terms))).order_by(desc(Keluarga.created_at))
            else:
                query_stmt = select(budi_models.Keluarga).where(Keluarga.keluarga_name =='Budi').order_by(desc(Keluarga.created_at))
            
            log.info(query_stmt)
            proxy_rows = await session.execute(query_stmt)
            result = proxy_rows.scalars().all()
            datas = jsonable_encoder(result)
            cucu = []
            for i in datas:
                dt = {
                    'nama_keluarga': i['keluarga_name'],
                    'nama_cucu': i['keluarga_cucu'],
                }
                cucu.append(dt)

            status = 'Data cucu Budi' if datas not in (None, []) else 'Data tidak ditemukan'

            return ResponseOutCustom(message_id="00", status=status, list_data=cucu)
        
        except Exception as e:
            return ResponseOutCustom(message_id="03", status=f'{e}', list_data=[])

async def get_data_cucu_perempuan_budi(keyword, db: AsyncSession):
    async with db as session:
        try:
            terms = []
            if keyword not in (None, [],''):
                terms.append(
                    or_(
                        (Keluarga.keluarga_anak.ilike(f'%{keyword}%')),
                        (Keluarga.keluarga_cucu.ilike(f'%{keyword}%')),
                        (Keluarga.keluarga_jenis_kelamin.ilike(f'%{keyword}%')),
                    )
                )
            if terms not in (None, []):
                query_stmt = select(budi_models.Keluarga).filter(
                    and_(
                        Keluarga.keluarga_name =='Budi', 
                        Keluarga.keluarga_jenis_kelamin_cucu == 'P',
                         *(terms))
                    ).order_by(desc(Keluarga.created_at))
            else:
                query_stmt = select(budi_models.Keluarga).where(
                    Keluarga.keluarga_name =='Budi',
                    Keluarga.keluarga_jenis_kelamin_cucu == 'P',
                ).order_by(desc(Keluarga.created_at))
            
            log.info(query_stmt)
            proxy_rows = await session.execute(query_stmt)
            result = proxy_rows.scalars().all()
            datas = jsonable_encoder(result)
            cucu = []
            for i in datas:
                dt = {
                    'nama_keluarga': i['keluarga_name'],
                    'nama_cucu': i['keluarga_cucu'],
                }
                cucu.append(dt)

            status = 'Data cucu Budi' if datas not in (None, []) else 'Data tidak ditemukan'

            return ResponseOutCustom(message_id="00", status=status, list_data=cucu)
        
        except Exception as e:
            return ResponseOutCustom(message_id="03", status=f'{e}', list_data=[])

        
async def get_data_bibi_farah(db: AsyncSession):
    async with db as session:
        try:
            
            query_stmt = select(budi_models.Keluarga).where(and_(Keluarga.keluarga_name =='Budi', Keluarga.jenis_kelamin_anak=='P')).order_by(desc(Keluarga.created_at))
            
            log.info(query_stmt)
            proxy_rows = await session.execute(query_stmt)
            result = proxy_rows.scalars().all()
            datas = jsonable_encoder(result)
            bibi = []
            for i in datas:
                dt = {
                    'nama_keluarga': i['keluarga_name'],
                    'nama_bibi_farah': i['keluarga_anak'],
                }
                bibi.append(dt)

            message_id = "00" if datas not in (None, []) else "02"
            status = 'Data bibi Farah' if datas not in (None, []) else 'Data tidak ditemukan'

            return ResponseOutCustom(message_id=message_id , status=status, list_data=bibi)
        
        except Exception as e:
            return ResponseOutCustom(message_id="03", status=f'{e}', list_data=[])
        
async def get_data_sepupu_laki_hani(db: AsyncSession):
    async with db as session:
        try:
            
            query_stmt = select(budi_models.Keluarga).where(and_(Keluarga.keluarga_name =='Budi', Keluarga.keluarga_jenis_kelamin_cucu=='L')).order_by(desc(Keluarga.created_at))
            
            log.info(query_stmt)
            proxy_rows = await session.execute(query_stmt)
            result = proxy_rows.scalars().all()
            datas = jsonable_encoder(result)
            bibi = []
            for i in datas:
                dt = {
                    'nama_keluarga': i['keluarga_name'],
                    'nama_sepupu_laki_hani': i['keluarga_cucu'],
                }
                bibi.append(dt)

            status = 'Data bibi Farah' if datas not in (None, []) else 'Data tidak ditemukan'

            return ResponseOutCustom(message_id="00", status=status, list_data=bibi)
        
        except Exception as e:
            return ResponseOutCustom(message_id="03", status=f'{e}', list_data=[])


async def update_data_cucu(data:UpdateCucuBudi,db: AsyncSession):
    async with db as session:
        try:
            
            # get data anak
            tbb = budi_models.Keluarga
            query_stmt = select(tbb).where(tbb.keluarga_anak==data.nama_anak)
            
            log.info(query_stmt)
            proxy_rows = await session.execute(query_stmt)
            result = proxy_rows.scalars().first()

            if result in (None, []):
                return ResponseOutCustom(message_id="02", status=f'Anak dengan nama {data.nama_anak} tidak ditemukan', list_data=[])

            update_data_cucu = update(tbb).where(tbb.keluarga_anak==data.nama_anak).values(
                keluarga_cucu = data.nama_cucu,
                keluarga_jenis_kelamin_cucu = data.jenis_kelamin_cucu
            )
            await session.execute(update_data_cucu)
            await session.commit()

            return ResponseOutCustom(message_id="00", status=f"Updata Cucu anak dari {data.nama_anak} Success", list_data=data)
        
        except Exception as e:
            return ResponseOutCustom(message_id="03", status=f'{e}', list_data=[])
        

async def delete_data_cucu(data:DeleteCucuBudi,db: AsyncSession):
    async with db as session:
        try:
            
            # get data anak
            tbb = budi_models.Keluarga
            query_stmt = select(tbb).where(tbb.keluarga_anak==data.nama_anak)
            
            log.info(query_stmt)
            proxy_rows = await session.execute(query_stmt)
            result = proxy_rows.scalars().all()

            if result in (None, []):
                return ResponseOutCustom(message_id="02", status=f'Failed, Anak dengan nama {data.nama_anak} tidak ditemukan', list_data=[])

            data_cucu = []
            for i in jsonable_encoder(result):
                data_cucu.append(i['keluarga_cucu'])

            print(data_cucu)

            if data.nama_cucu not in data_cucu:
                return ResponseOutCustom(message_id="02", status=f"Failed, Nama cucu tidak terdaftar sebagai anak dari {data.nama_anak}, harap memasukkan data yang benar", list_data=data)


            delete_data_cucu = delete(tbb).where(tbb.keluarga_cucu==data.nama_cucu)
            await session.execute(delete_data_cucu)
            await session.commit()

            return ResponseOutCustom(message_id="00", status=f"Delete Cucu anak dari {data.nama_anak} Success", list_data=data)
        
        except Exception as e:
            return ResponseOutCustom(message_id="03", status=f'{e}', list_data=[])
        
        
async def visualisasi_tree_keluarga_budi(db: AsyncSession):
    async with db as session:
        try:

            graph = Graph(engine='dot')
            graph = Digraph('Tree')
            graph.node('Dedi')
            
            # get data anak
            tbb = budi_models.Keluarga
            query_stmt = select(tbb).order_by(desc(tbb.created_at))
            
            log.info(query_stmt)
            proxy_rows = await session.execute(query_stmt)
            data = proxy_rows.scalars().all()

            if data in (None, []):
                return ResponseOutCustom(message_id="02", status=f'Failed, Data tidak ditemukan tidak ditemukan', list_data=[])

            for row in jsonable_encoder(data):
                node = row['keluarga_anak']
                leaf = row['keluarga_name']
                if not graph.node(node):
                    graph.node(node, shape='ellipse', style='filled', fillcolor='lightblue')
                graph.node(leaf, shape='box', style='filled', fillcolor='lightgrey')
                graph.edge(node, leaf)

            graph.view()
        
        except Exception as e:
            return ResponseOutCustom(message_id="03", status=f'{e}', list_data=[])