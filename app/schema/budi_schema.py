from pydantic import BaseModel

class BudiFamily(BaseModel): #buat test create user
    keluarga_name : str
    keluarga_anak : str
    jenis_kelamin_anak : str
    keluarga_cucu : str
    keluarga_jenis_kelamin_cucu : str


class UpdateCucuBudi(BaseModel): #buat test create user
    nama_anak : str
    nama_cucu : str
    jenis_kelamin_cucu : str
    

class DeleteCucuBudi(BaseModel): #buat test create user
    nama_anak : str
    nama_cucu : str
    


