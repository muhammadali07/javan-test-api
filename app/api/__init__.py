from fastapi import APIRouter

from .budi_api import *

api_router = APIRouter()

api_router.include_router(budi_api.router, prefix='/budi', tags=['Payment Core'])

