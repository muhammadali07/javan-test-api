from schema import BudiFamily, UpdateCucuBudi, DeleteCucuBudi
from fastapi import Depends, APIRouter
from sqlalchemy.ext.asyncio import AsyncSession
from service import get_async_session
from crud import budi_crud

router = APIRouter()

@router.post("/create_data_family", )
async def create_data_family(
    data: BudiFamily, 
    db: AsyncSession = Depends(get_async_session)
    ):
    out_resp = await budi_crud.create_data_family(data, db)
    return out_resp

@router.get("/get_data_family", )
async def get_data_family(
    db: AsyncSession = Depends(get_async_session),
    limit : int = 10,
    page : int = 0,
    keyword : str = ''
    ):
    out_resp = await budi_crud.get_data_family(limit, page, keyword, db)
    return out_resp



@router.get("/get_data_anak_budi", )
async def get_data_anak_budi(
    db: AsyncSession = Depends(get_async_session),
    keyword : str = ''
    ):
    out_resp = await budi_crud.get_data_anak_budi(keyword, db)
    return out_resp


@router.get("/get_all_data_cucu_budi", )
async def get_data_cucu_budi(
    db: AsyncSession = Depends(get_async_session),
    keyword : str = ''
    ):
    out_resp = await budi_crud.get_data_cucu_budi(keyword, db)
    return out_resp

@router.get("/get_data_cucu_perempuan_budi", )
async def get_get_data_cucu_perempuan_budidata_cucu_budi(
    db: AsyncSession = Depends(get_async_session),
    keyword : str = ''
    ):
    out_resp = await budi_crud.get_data_cucu_perempuan_budi(keyword, db)
    return out_resp


@router.get("/get_data_bibi_farah", )
async def get_data_bibi_farah(
    db: AsyncSession = Depends(get_async_session)
    ):
    out_resp = await budi_crud.get_data_bibi_farah(db)
    return out_resp


@router.get("/get_data_sepupu_laki_hani", )
async def get_data_sepupu_laki_hani(
    db: AsyncSession = Depends(get_async_session)
    ):
    out_resp = await budi_crud.get_data_sepupu_laki_hani(db)
    return out_resp


@router.put("/update_data_cucu", )
async def update_data_cucu(
    data: UpdateCucuBudi,
    db: AsyncSession = Depends(get_async_session)
    ):
    out_resp = await budi_crud.update_data_cucu(data,db)
    return out_resp


@router.delete("/delete_data_cucu", )
async def delete_data_cucu(
    data: DeleteCucuBudi,
    db: AsyncSession = Depends(get_async_session)
    ):
    out_resp = await budi_crud.delete_data_cucu(data,db)
    return out_resp

@router.get("/visualisasi_tree_keluarga_budi", )
async def visualisasi_tree_keluarga_budi(
    db: AsyncSession = Depends(get_async_session)
    ):
    out_resp = await budi_crud.visualisasi_tree_keluarga_budi(db)
    return out_resp

